<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Loan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Loan', function (Blueprint $table) {
      //  $table->engine = 'InnoDB';
          $table->increments('id');
          $table->integer('sid');
        /*  $table->foreign('sid')
          ->references('id')->on('users')
          ->onDelete('cascade');*/
          $table->integer('bid');
        /*  $table->foreign('bid')
          ->references('id')->on('Books')
          ->onDelete('cascade');*/
          $table->integer('statue');
          $table->date('start');
          $table->date('end');


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
