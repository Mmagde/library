<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Student extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Student', function (Blueprint $table) {
      //  $table->engine = 'InnoDB';
          $table->integer('id');
        /*  $table->foreign('id')
          ->references('id')->on('users')
          ->onDelete('cascade');*/
          $table->integer('gid');
        /*  $table->foreign('gid')
          ->references('id')->on('Grades')
          ->onDelete('cascade');*/
          $table->string('isbanned')->default(0);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
