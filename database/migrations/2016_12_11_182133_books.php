<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Books extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Books', function (Blueprint $table) {
      //  $table->engine = 'InnoDB';
          $table->increments('id');
          $table->string('title');
          $table->string('isbn');
          $table->text('description');
          $table->integer('ed');
          $table->string('ddc');
          $table->integer('numberofcopies');
          $table->integer('prefixid');



      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
