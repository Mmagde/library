<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'name', 'nid', 'password','rid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function student()
    {
      //return $this->hasOne('App\Student', 'id', 'id');
        return Student::where('id', $this->id)->first();
    }
    public function role()
    {
      //return $this->hasOne('App\Student', 'id', 'id');
        return role::where('id', $this->rid)->first();
    }
}
