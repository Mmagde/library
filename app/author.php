<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class author extends Model
{
    protected $table = 'Author';
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];
}
