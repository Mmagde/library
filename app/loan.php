<?php

namespace App;
use App\book;
use App\Student;
use Illuminate\Database\Eloquent\Model;

class loan extends Model
{
  protected $table = 'Loan';
  public $timestamps = false;
  protected $fillable = [
      'sid', 'bid', 'statue','start','end',
  ];
  public function book()
  {
     return book::where('id',$this->bid)->first();
  }
  public function student()
  {
     return Student::where('id',$this->sid)->first();
  }
}
