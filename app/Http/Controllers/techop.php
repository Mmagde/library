<?php

namespace App\Http\Controllers;
use App\author;
use App\book;
use App\book_author;
use App\section;
use App\loan;
use Illuminate\Http\Request;

class techop extends muser
{
  public function show_books()
  {
    $books = book::paginate(10);
    return view('techop.showbooks',compact('books'));
  }
  public function show_authors()
  {
    $authors = author::paginate(10);
    return view('techop.showauthors',compact('authors'));
  }
  public function add_bookv()
  {
    $authors = author::all();
    return view('techop.addbook',compact('authors'));
  }
  public function add_book(Request $request)
  {
    $this->validate($request, [
      'title'             => 'required' ,                       // just a normal required validation
      'isbn'              => 'required' ,
      'description'       => 'required'  ,
      'ed'                => 'required|numeric'  ,
      'ddc'               => 'required'  ,
      'numberofcopies'    => 'required|numeric'  ,
      'authors'           => 'required'
    ]);
    $section = section::where('prefix', $request->ddc[0])->first();
    $book = book::create([
      'title'             => $request->title ,                       // just a normal required validation
      'isbn'              => $request->isbn ,
      'description'       => $request->description ,
      'ed'                => $request->ed ,
      'ddc'               => $request->ddc ,
      'numberofcopies'    => $request->numberofcopies ,
      'prefixid'          => $section->id
    ]);
    foreach ($request->authors as $author) {
        book_author::create([
          'aid'               => $author ,
          'bid'               => $book->id
        ]);
    }

    return back()->with('status', 'Added');

  }
  public function remove_book($id)
  {
    book::destroy($id);
    book_author::where('bid', $id)->delete();
    loan::where('bid', $id)->delete();
    return back()->with('status', 'Deleted');
  }
  public function update_bookv($id)
  {
    $book = book::where('id',$id)->first();
    $b_authors = book_author::where('bid', $id)->get();
    $book_authors = array();
    foreach ($b_authors as $book_author) {
       array_push($book_authors , $book_author->author());
    }
    $authors = author::all();
    return view('techop.updatebook',compact('book','book_authors','authors'));
  }
  public function update_book(Request $request)
  {
    $this->validate($request, [
      'title'             => 'required' ,                       // just a normal required validation
      'isbn'              => 'required' ,
      'description'       => 'required'  ,
      'ed'                => 'required|numeric'  ,
      'ddc'               => 'required'  ,
      'numberofcopies'    => 'required|numeric'  ,
      'authors'           => 'required'
    ]);

    $book = book::where('id', $request->id)->first();
    $section = section::where('prefix', $request->ddc[0])->first();
    $book->title             = $request->title ;                       // just a normal required validation
    $book->isbn              = $request->isbn ;
    $book->description       = $request->description ;
    $book->ed                = $request->ed ;
    $book->ddc               = $request->ddc ;
    $book->numberofcopies    = $request->numberofcopies;
    $book->prefixid          = $section->id;
    $book->save();
    book_author::where('bid', $request->id)->delete();
    foreach ($request->authors as $author) {
        book_author::create([
          'aid'               => $author ,
          'bid'               => $book->id
        ]);
      }
    return back()->with('status', 'Updated');
  }
  public function add_authorv()
  {
    return view('techop.addauthor');
  }
  public function add_author(Request $request)
  {
    $this->validate($request, [
      'name'             => 'required'                      // just a normal required validation
    ]);
    $user = author::create(['name' =>  $request->name
    ]);
     return back()->with('status', 'Added');
  }
  public function update_authorv($id)
  {
    $author = author::where('id',$id)->first();
    return view('techop.updateauthor',compact('author'));
  }
  public function update_author(Request $request)
  {
    $this->validate($request, [
      'name'             => 'required'                      // just a normal required validation
    ]);
    $author = author::where('id',$request->id)->first();
    $author->name =  $request->name;
    $author->save();
    return back()->with('status', 'updated');
  }
  public function delete_author($id)
  {
    author::destroy($id);
    /*$books = book_author::where('aid', $id)->get();
    foreach ($books->all() as $book) {
      book::destroy($book->id);
    }*/
    book_author::where('aid', $id)->delete();
    return back()->with('status', 'Deleted');
  }
}
