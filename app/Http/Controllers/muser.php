<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Student;
use App\book;
use App\loan;
use App\user;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class muser extends Controller
{

    public function login(Request $request)
    {
      if (Auth::attempt(['nid' => $request->nid, 'password' => $request->password ])) {
          ban();
          if(Auth::user()->rid == 1 )  return redirect()->intended('/student');
          if(Auth::user()->rid == 2 )  return redirect()->intended('/manager');
          if(Auth::user()->rid == 3 )  return redirect()->intended('/techop');
          if(Auth::user()->rid == 4 )  return redirect()->intended('/loaning');
        }
        return back()->with('error', 'Invalid National id or password');
    }
    public function changepass(Request $request)
    {
      $this->validate($request, [
        'oldpassword'             => 'required',                        // just a normal required validation
        'newpassword'              => 'required',     // required and must be unique in the ducks table
        'confirmpassword'         => 'required|same:newpassword'
      ]);
      $user = user::where('id', Auth::user()->id)->first();
      if (Hash::check($request->oldpassword , $user->password))
        {
          $user->password = bcrypt($request->newpassword);
          $user->save();
          return back()->with('done', 'password changed successfully');
        }
      else
      return back()->with('error', 'old password is wrong');
    }

    public function logout()
    {
       Auth::logout();
       return redirect('/');
    }

}
