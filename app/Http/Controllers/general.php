<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\section;
use App\book;
class general extends Controller
{
  public function index()
  {
    $secs = section::all();
    $sections = $secs->all();
    return view('general.home',compact('sections'));
  }
  public function search(Request $request)
  {
     $q = $request->q;
     $search = book::search($q, null, true)->paginate(10);
     return view('general.search',compact('search'));
  }
  /*public function show_catalog()
  {
    # code...
  }*/
  public function show_section($id)
  {
    $books = book::where('prefixid',$id)
    ->orderBy('ddc', 'ASC')
    ->paginate(10);
    $section = section::where('id',$id)->first();
    return view('general.section',compact('section','books'));

  }
  public function show_bookdetails($id)
  {
     $book = book::where('id' , $id)->first();
     return view('general.details',compact('book'));
  }

}
