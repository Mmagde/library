<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\loan;
use App\book;
use App\Student;
class loaning extends muser
{

    public function notActive()
    {

      $title = "Not active reservations";
      $loans = loan::where('statue',0)->paginate(10);
      return view('loaning.notactive',compact('loans','title'));
    }
    public function active()
    {
      $title = "Active reservations";
      $loans = loan::where('statue',1)->paginate(10);
      return view('loaning.active',compact('loans','title'));
    }
    public function completed()
    {
      $title = "Completed reservations";
      $loans = loan::where('statue',2)->paginate(10);
      return view('loaning.complete',compact('loans','title'));
    }
    public function lostbooks()
    {
      $title = "Lost Books";
      $loans = loan::where('statue',-1)->paginate(10);
      return view('loaning.lostbooks',compact('loans','title'));
    }
    public function activate_reservation($lid)
    {
      $loan = loan::where('id' , $lid)->first();
      $loan->statue = 1;
      $loan->save();
      return back()->with('status', 'Done');
    }
    public function complete_reservation($lid)
    {
      $loan = loan::where('id' , $lid)->first();
      $loan->statue = 2;
      $loan->save();
      $book = book::where('id', $loan->bid)->first();
      $book->numberofcopies = $book->numberofcopies + 1;
      $book->save();
      return back()->with('status', 'Done');

    }
    public function show_banlist()
    {
      $students = Student::where('isbanned' , 1)->paginate(10);
      return view('loaning.ban',compact('students'));
    }
    public function unban($sid)
    {
      $student = Student::where('id' , $sid)->first();
      $student->isbanned = 0;
      $student->save();
      return back()->with('status', 'Done');
    }
}
