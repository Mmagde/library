<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Http\RedirectResponse;
use Validator;
use App\User;
use App\loan;
use App\Student;
use Charts;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class manager extends muser
{
      public function add_studentv()
      {
        return view('manager.addstudent');
      }
      public function add_student(Request $request)
      {
        $this->validate($request, [
          'name'             => 'required',                        // just a normal required validation
          'nid'              => 'required|unique:users|numeric|digits:14',     // required and must be unique in the ducks table
          'password'         => 'required',
          'password_confirm' => 'required|same:password'
        ]);
      /*  $rules = array(
             'name'             => 'required',                        // just a normal required validation
             'nid'              => 'required|unique:users',     // required and must be unique in the ducks table
             'password'         => 'required',
             'password_confirm' => 'required|same:password'           // required and has to match the password field
         );
        $validator = Validator::make($request->all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

             return redirect('/manager/addstudent');
        }*/
        $user = User::create(['name' =>  $request->name,
        'nid' => $request->nid,
        'password' => bcrypt($request->password),
        'rid' => 1
        ]);

        $student = Student::create([
        'id'  => $user->id ,
        'gid' => $request->gid
        ]);

         return redirect('manager/addstudent')->with('status', 'Profile Made');


      }
      public function add_employeev()
      {
        return view('manager.addemployee');
      }
      public function add_employee(Request $request)
      {
        $this->validate($request, [
          'name'             => 'required',                        // just a normal required validation
          'nid'              => 'required|unique:users|numeric|digits:14',     // required and must be unique in the ducks table
          'password'         => 'required',
          'password_confirm' => 'required|same:password'
        ]);
        $user = User::create(['name' =>  $request->name,
        'nid' => $request->nid,
        'password' => bcrypt($request->password),
        'rid' => $request->rid
        ]);
         return redirect('manager/addemployee')->with('status', 'Profile Made');

      }
      public function make_report()
      {
        $month = Carbon::today()->subMonth();
        $loans = loan::whereIn('statue',[0,1,2])
        ->whereDate('start', '>=', $month)
        ->select(DB::raw('count(*) as count, bid'))
        ->groupBy('bid')->get();//select('*','count(*)');
        //return var_dump($loans->all());
        $data = array();
        foreach ($loans->all() as $loan) {
          $name = $loan->book()->title;
          $data[$name] = $loan->count;
        }
        $chart = Charts::create('bar', 'highcharts')
            //->view('custom.line.chart.view') // Use this if you want to use your own template
            ->title('Mostly loaned books')
            ->labels(array_keys($data))
            ->values(array_values($data))
            ->dimensions(1000,500)
            ->responsive(true);
        $loans2 = loan::where('statue',-1)
        ->whereDate('start', '>=', $month)
        ->select(DB::raw('count(*) as count, bid'))
        ->groupBy('bid')->get();//select('*','count(*)');
        //return var_dump($loans->all());
        $data2 = array();
        foreach ($loans2->all() as $loan) {
          $name = $loan->book()->title;
          $data2[$name] = $loan->count;
        }
        $chart2 = Charts::create('bar', 'highcharts')
            //->view('custom.line.chart.view') // Use this if you want to use your own template
            ->title('Lost books')
            ->labels(array_keys($data2))
            ->values(array_values($data2))
            ->dimensions(1000,500)
            ->responsive(true);

         return view('manager.report', ['chart' => $chart , 'chart2' => $chart2]);
      }
      public function show_students()
      {
        $users  = User::where('rid', 1)->paginate(10);
        return view('manager.showstudents',compact('users'));
      }
      public function show_employees()
      {
        $users  = User::whereIn('rid', [3,4])->paginate(10);
        return view('manager.showemployees',compact('users'));
      }
      public function remove($id)
      {
        User::destroy($id);
        return back()->with('status', 'Deleted');
      }

      public function reset($id)
      {
        $user  = User::where('id', $id)->first();
        $string = str_random(8);
        $user->password = bcrypt($string);
        $user->save();
        return back()->with('status', 'reseted , the new password is '.$string);
      }



}
