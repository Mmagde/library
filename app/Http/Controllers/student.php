<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Student as students;
use App\book;
use App\loan;
use Carbon\Carbon;
class student extends muser
{
  public function reserve($bid)
  {
     //checking if banned
     $student = Students::where('id', Auth::user()->id)->first();
     if($student->isbanned == 1 ) return back()
     ->with('status', 'you are banned from loaning contact the loaning employee to solve this');
     //checking if reached the limit
     $current_loans = loan::where('sid', Auth::user()->id)->whereIn('statue', [0, 1])->count();
     if($current_loans >= $student->grade()->bookslimit) return back()
     ->with('status', 'Already reached the loaning limit');
     //creating loan
     $loan = loan::create([
     'bid' =>  $bid,
     'sid' => $student->id,
     'statue' => 0,
     'start' => Carbon::today(),
     'end'   => Carbon::today()->addDays($student->grade()->dayslimit)
     ]);
     $book = book::where('id', $bid)->first();
     $book->numberofcopies = $book->numberofcopies - 1;
     $book->save();
     return back()
     ->with('status', 'Reserved : contact the loaning employee to complete reservation');

  }
  public function show_reservations()
  {
    $loans = loan::where('sid', Auth::user()->id)->paginate(10);
    return view('student.reservations' , compact('loans'));
  }
}
