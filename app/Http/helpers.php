<?php
use App\Student;
use App\book;
use App\loan;
use Carbon\Carbon;
function ban()
{
  $loans = loan::whereIn('statue', [0, 1])->get();
  $current = Carbon::today();
  foreach ($loans->all() as $loan) {
      $end = Carbon::createFromFormat('Y-m-d', $loan->end);
      $diff = $current->diffInDays($end);
      if($diff < 0){
        $student = Student::where('id', $loan->sid);
        $student->isbanned = 1;
        $student->save();
        if($loan->statue == 0)
        {
          $book = book::where('id', $loan->$bid)->first();
          $book->numberofcopies = $book->numberofcopies + 1;
          $book->save();
          $loan->delete();
          //$loan->save();
        }else{
          $loan->statue = -1;
          $loan->save();
        }
      }
  }
}
