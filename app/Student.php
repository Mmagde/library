<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  public $timestamps = false;
  protected $table = 'Student';
  protected $fillable = [
      'id', 'gid',
  ];
  public function user() {
      return User::where('id', $this->id)->first();
   }
  public function grade()
  {
      return grade::where('id', $this->gid)->first();
  }
}
