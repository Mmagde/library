<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class book extends Model
{
    use SearchableTrait;


    protected $table = 'Books';
    public $timestamps = false;
    protected $fillable = [
        'title', 'isbn', 'description','ed','ddc','numberofcopies','sid','prefixid',
    ];
    protected $searchable = [
     'columns' => [
         'title' => 10,
         'isbn' => 10,

     ],
    ];

    public function book_author()
    {
      //return $this->hasOne('App\Student', 'id', 'id');
        return book_author::where('bid', $this->id)->get();
    }

}
