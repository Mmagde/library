<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book_author extends Model
{
  protected $table = 'Book_Author';
  public $timestamps = false;
  protected $fillable = [
      'aid', 'bid',
  ];
  public function author()
  {
    //return $this->hasOne('App\Student', 'id', 'id');
      return author::where('id', $this->aid)->first();
  }
}
