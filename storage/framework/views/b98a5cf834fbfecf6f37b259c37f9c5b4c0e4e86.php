<ul class="sidebar-menu">
  <li class="header">MAIN</li>
  <li class="treeview">
    <a href="#">
      <i class="fa "></i> <span>Studens</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo e(url('/manager/addstudent')); ?>"></i>Add new</a></li>
      <li><a href="<?php echo e(url('/manager/students')); ?>"></i>Show</a></li>
   </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa "></i> <span>Employees</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="<?php echo e(url('/manager/addemployee')); ?>"></i>Add new</a></li>
      <li><a href="<?php echo e(url('/manager/employees')); ?>"></i>Show</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="<?php echo e(url('/manager/report')); ?>">
      <i class="fa "></i> <span>Generate report</span>
    </a>
  </li>
</ul>
