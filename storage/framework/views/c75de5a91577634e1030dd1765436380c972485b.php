       <?php $__env->startSection('sidebar'); ?>
        <?php echo $__env->make('manager.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">

  <div class="col-md-6" style="margin-top:20px;">

    <?php if(count($errors) > 0): ?>
      <div class="alert alert-danger alert-dismissible" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </ul>
      </div>
    <?php endif; ?>
    <?php if(session('status')): ?>
        <div class="alert alert-success alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <h4><i class="icon fa fa-check"></i> success</h4>
               <?php echo e(session('status')); ?>

        </div>
    <?php endif; ?>
    <form action="<?php echo e(url('/manager/addemployee')); ?>" method="post" style="margin-top:20px;">
      <?php echo e(csrf_field()); ?>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Full name" name="name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="National id" name="nid">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password" name="password_confirm">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
      Department
      <select class="form-control" name="rid">
            <option value="3">Technical operation</option>
            <option value="4">loaning</option>
     </select>
     </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.form-box -->
  </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('mviews.includes.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>