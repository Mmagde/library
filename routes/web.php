<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/','general@index');

/* manager routes */
Route::group( ['prefix' => 'manager' , 'middleware' => ['auth','Role:2']] , function () {

  Route::get('/', function(){
    return view('manager.main');
  });
  Route::get('/students','manager@show_students');
  Route::get('/employees','manager@show_employees');
  Route::get('/addstudent','manager@add_studentv');
  Route::post('/addstudent','manager@add_student');
  Route::get('/addemployee','manager@add_employeev');
  Route::post('/addemployee','manager@add_employee');
  Route::get('/remove/{id}','manager@remove');
  Route::get('/reset/{id}','manager@reset');
  Route::get('/report','manager@make_report');




});
/* techop routes */
Route::group( ['prefix' => 'techop' , 'middleware' => ['auth','Role:3']] , function () {

  Route::get('/', function(){
    return view('techop.main');
  });
  Route::get('/books','techop@show_books');
  Route::get('/authors','techop@show_authors');
  Route::get('/addbook','techop@add_bookv');
  Route::get('/addauthor','techop@add_authorv');
  Route::post('/addbook','techop@add_book');
  Route::post('/addauthor','techop@add_author');
  Route::post('/updateauthor','techop@update_author');
  Route::post('/updatebook','techop@update_book');
  Route::get('/updateauthor/{id}','techop@update_authorv');
  Route::get('/deleteauthor/{id}','techop@delete_author');
  Route::get('/updatebook/{id}','techop@update_bookv');
  Route::get('/deletebook/{id}','techop@remove_book');




});
/* loaning routes */
Route::group( ['prefix' => 'loaning' , 'middleware' => ['auth','Role:4']] , function () {

  Route::get('/', function(){
    return view('loaning.main');
  });
  Route::get('/notActive', 'loaning@notActive');
  Route::get('/active', 'loaning@active');
  Route::get('/completed', 'loaning@completed');
  Route::get('/lostbooks', 'loaning@lostbooks');
  Route::get('/activate/{lid}', 'loaning@activate_reservation');
  Route::get('/complete/{lid}', 'loaning@complete_reservation');
  Route::get('/showbanlist', 'loaning@show_banlist');
  Route::get('/unban/{sid}', 'loaning@unban');



});
/* student routes */
Route::group( ['prefix' => 'student' , 'middleware' => ['auth','Role:1']] , function () {
  Route::get('/', function(){
    return view('student.main');
  });
  Route::get('/reserve/{bid}','student@reserve');
  Route::get('/reservations','student@show_reservations');


});
/* general routes */

Route::group( ['middleware' => ['auth']] , function () {

Route::get('/logout','muser@logout');
Route::get('/changepass',function(){
  return view('auth.changepass');
});
Route::post('/changepass','muser@changepass');
Route::get('/section/{id}','general@show_section');
Route::get('/book/{id}','general@show_bookdetails');
Route::get('/search','general@search');

});

Route::group( ['middleware' => ['guest']] , function () {
Route::get('/login', function(){
  return view('auth.login');
});
Route::post('/login','muser@login');
});
