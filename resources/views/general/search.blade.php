<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>

   @include('general.css')
  </head>
  <body>

  @include('general.nav')

  <div id="section" >
  <div class="container" >
      <div class="row">
        <div class="col-md-4">

        <form action="{{url('/search')}}" method="get">

          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for book" name="q">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div><!-- /input-group -->
          {{csrf_field()}}
         </form>
      </div>
    </div>
      <hr>
  @if(count($search->all()) > 0)
      <div class="row" >
            <h2>{{count($search->all())}} result</h2>
      </div>
       @foreach ($search->all() as $book)
      <div class="row" >
        <div class="col-md-2">
          <a href="{{url('/book/'.$book->id)}}" class="thumbnail">
             <img src="{{asset('front/book.jpg')}}" alt="{{$book->title}}">
          </a>
        </div>
        <div class="col-md-4">
          <h3>{{$book->title}}</h3>
          <h5><b>Classification number : {{$book->ddc}}</b></h5>
          <h5>{{str_limit($book->description,50)}} ..... </h5>
          <a href="{{url('/book/'.$book->id)}}" >see more</a>
        </div>

     </div>
     @endforeach
  {{$search->links()}}
  @else
  <div class="row" >
        <h2>No Matches found</h2>
        <hr>
  </div>
  @endif

  </div>







  @include('general.js')
  </body>
</html>
