<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>

   @include('general.css')
  </head>
  <body>

  @include('general.nav')

  <div id="reserve" >
  <div class="container wrapper" >

      <div class="row" >
            <h2>{{$book->title}}</h2>
            <hr>
            @if (session('status'))
                <div class="alert alert-warning alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  {{ session('status') }}
                </div>
            @endif
      </div>

      <div class="row" >
        <div class="col-md-4">
          <a href="#" class="thumbnail">
             <img src="{{asset('front/book.jpg')}}" alt="{{$book->title}}">
          </a>
        </div>


        <div class="col-md-6">
          <h5><b>Classification number : {{$book->ddc}}</b></h5>
          <h5><b>ISBN : {{$book->isbn}}</b></h5>
          <h5><b>Edition : {{$book->ed}}</b></h5>
          <h5><b> Author(s) :
          @foreach ($book->book_author() as $book_author)
              @if ($loop->last)
                 {{$book_author->author()->name}}
              @else
                 {{$book_author->author()->name . ','}}
              @endif
          @endforeach
         </b></h5>
         <p class="prob"><b>Description</b> : {{$book->description}}</p>
          @if($book->numberofcopies > 3 && Auth::user()->rid == 1)
          <a href="{{url('/student/reserve/'.$book->id)}}" class="btn btn-primary but pull-right">Reserve</a>
          @endif
        </div>
        </div>




  </div>
  </div>


  @include('general.js')
  </body>
</html>
