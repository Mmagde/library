<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>

   @include('general.css')
  </head>
  <body>

  @include('general.nav')
  <div id="header" >
  <div class="container" >

      <div class="row" >


          <div class="col-xs-4 col-xs-push-4 logo">
             <h3>Search for book</h3>
            <form method="get" action="{{url('/search')}}" >
              <div class="input-group">

                <input type="text" class="form-control" placeholder="Search for book" name="q">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </span>
              </div><!-- /input-group -->
              {{csrf_field()}}
             </form>


          </div>
     </div>
  </div>
</div>
          <div id="who-we" >
          <div class="container" >

              <div class="row" >

                  <h2>Catalog</h2>
                  <div class=" col-md-1">
                  </div>
                  @for ($i = 0; $i < 5; $i++)

                      <div class="col-md-2">
                        <a href="{{url('/section/'.$sections[$i]->id)}}" class="thumbnail">
                           <img src="{{asset('front/book.jpg')}}" alt="{{$sections[$i]->name}}">

                        </a>
                        <h5>{{$sections[$i]->name}}</h5>
                        <h6>{{$sections[$i]->prefix}}00</h6>
                      </div>

                  @endfor

                </div>
                <div class="row">
                  <div class=" col-md-1">
                  </div>
                  @for ($i = 5; $i < 10; $i++)

                      <div class="col-md-2">
                        <a href="{{url('/section/'.$sections[$i]->id)}}" class="thumbnail">
                           <img src="{{asset('front/book.jpg')}}" alt="{{$sections[$i]->name}}">

                        </a>
                        <h5>{{$sections[$i]->name}}</h5>
                        <h6>{{$sections[$i]->prefix}}00</h6>
                      </div>

                  @endfor

              </div>

          </div>
          </div>





  @include('general.js')
  </body>
</html>
