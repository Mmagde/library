
@extends('mviews.includes.main')
       @section('sidebar')
        @include('manager.sidebar')
       @endsection

@section('content')
        {!! Charts::assets() !!}
        <div class="content-wrapper">
          <div class="row">
          <div class="col-md-10 col-md-push-1" style="margin-top:20px;">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Mostly loaned books</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                  {!! $chart->render() !!}
              </div>
            </div>
          </div>
         </div>
       </div>
       <div class="row">
       <div class="col-md-10 col-md-push-1" style="margin-top:20px;">
       <div class="box box-primary">
         <div class="box-header with-border">
           <h3 class="box-title">Lost books</h3>

           <div class="box-tools pull-right">
             <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
             </button>
           </div>
         </div>
         <div class="box-body">
           <div class="chart">
               {!! $chart2->render() !!}
           </div>
         </div>
       </div>
      </div>
    </div>
          </div
@endsection
