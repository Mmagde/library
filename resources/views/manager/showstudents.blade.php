
@extends('mviews.includes.main')
        @section('sidebar')
         @include('manager.sidebar')
        @endsection
       @section('content')
       <div class="content-wrapper">
         <div class="col-md-12" style="margin-top:20px;">
           @if (session('status'))
               <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> success</h4>
                      {{ session('status') }}
               </div>
           @endif
               <h3 class="box-title">Students</h3>
             </div>
             <!-- /.box-header -->
             <div class="box-body">
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Name</th>
                       <th>National id</th>
                       <th>Grade</th>
                       <th>Remove</th>
                       <th>Reset</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($users->all() as $user)
                       <tr>
                         <td>{{$user->name}}</td>
                         <td>{{$user->nid}}</td>
                         <td>{{$user->student()->grade()->name}}</td>
                         <td><a href="{{url('manager/remove/'.$user->id)}}" class="btn btn-block btn-danger">remove</a></td>
                         <td><a href="{{url('manager/reset/'.$user->id)}}" class="btn btn-block btn-warning">reset</a><td>
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$users->links()}}

          </div>
       </div>
       @endsection
