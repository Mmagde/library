
@extends('mviews.includes.main')
        @section('sidebar')
         @include('loaning.sidebar')
        @endsection
       @section('content')
       <div class="content-wrapper">
         <div class="col-md-6" style="margin-top:20px;">
           @if (session('status'))
               <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i>Alert</h4>
                      {{ session('status') }}
               </div>
           @endif
               <h3 class="box-title">Ban List</h3>
             </div>

             <!-- /.box-header -->

             <div class="box-body " >
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Name</th>
                       <th>National id</th>
                       <th>action</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($students->all() as $student)
                       <tr>
                         <td>{{$student->user()->name}}</td>
                         <td>{{$student->user()->nid}}</td>
                         <td><a href="{{url('loaning/unban/'.$student->id)}}" class="btn btn-block btn-primary">Unban</a><td>
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$students->links()}}

          </div>
       </div>
       @endsection
