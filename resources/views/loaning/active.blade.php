
@extends('mviews.includes.main')
        @section('sidebar')
         @include('loaning.sidebar')
        @endsection
       @section('content')
       <div class="content-wrapper">
         <div class="col-md-6" style="margin-top:20px;">
               @if (session('status'))
                   <div class="alert alert-success alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <h4><i class="icon fa fa-check"></i>Alert</h4>
                          {{ session('status') }}
                   </div>
               @endif
               <h3 class="box-title">{{$title}}</h3>
             </div>

             <!-- /.box-header -->

             <div class="box-body " >
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Student's national id</th>
                       <th>Book Name</th>
                       <th>reserved at</th>
                       <th>Ends at</th>
                       <th>action</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($loans->all() as $loan)
                       <tr>
                         <td>{{$loan->student()->user()->nid}}</td>
                         <td>{{$loan->book()->title}}</td>
                         <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $loan->start)->toFormattedDateString()}}</td>
                         <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $loan->end)->toFormattedDateString()}}</td>
                         <td> <a href="{{url('loaning/complete/'.$loan->id)}}" class="btn btn-block btn-primary">Complete</a></td>
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$loans->links()}}

          </div>
       </div>
       @endsection
