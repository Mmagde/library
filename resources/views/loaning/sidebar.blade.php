<ul class="sidebar-menu">
  <li class="header">MAIN</li>
  <li class="treeview">
    <a href="#">
      <i class="fa "></i> <span>Reservations</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{url('/loaning/notActive')}}"></i>Not active</a></li>
      <li><a href="{{url('/loaning/active')}}"></i>Active</a></li>
      <li><a href="{{url('/loaning/completed')}}"></i>Completed</a></li>

 </ul>
  </li>
  <li class="treeview">
    <a href="{{url('/loaning/showbanlist')}}">
      <i class="fa "></i> <span>Ban list</span>
    </a>
  </li>
  <li class="treeview">
    <a href="{{url('/loaning/lostbooks')}}">
      <i class="fa "></i> <span>Lost Books</span>
    </a>
  </li>
</ul>
