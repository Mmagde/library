
@extends('mviews.includes.main')
        @section('sidebar')
         @include('student.sidebar')
        @endsection
       @section('content')
       <div class="content-wrapper">
         <div class="col-md-6" style="margin-top:20px;">

               <h3 class="box-title">Reservations</h3>
             </div>

             <!-- /.box-header -->

             <div class="box-body " >
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Book Name</th>
                       <th>reserved at</th>
                       <th>Ends at</th>
                       <th>Statue</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($loans->all() as $loan)
                       <tr>
                         <td>{{$loan->book()->title}}</td>
                         <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $loan->start)->toFormattedDateString()}}</td>
                         <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $loan->end)->toFormattedDateString()}}</td>
                         @if($loan->statue == 0)
                          <td> <span class="label label-warning">Not active</span></td>
                         @elseif($loan->statue == 1)
                          <td> <span class="label label-info">Active</span></td>
                         @elseif($loan->statue == 2)
                          <td> <span class="label label-success">Complete</span></td>
                         @elseif($loan->statue == -1)
                          <td> <span class="label label-danger">book is marked as lost</span></td>
                         @endif
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$loans->links()}}

          </div>
       </div>
       @endsection
