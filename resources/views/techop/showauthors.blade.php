
@extends('mviews.includes.main')
        @section('sidebar')
         @include('techop.sidebar')
        @endsection
       @section('content')
       <div class="content-wrapper">
         <div class="col-md-6" style="margin-top:20px;">
           @if (session('status'))
               <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> success</h4>
                      {{ session('status') }}
               </div>
           @endif
               <h3 class="box-title">Authors</h3>
             </div>

             <!-- /.box-header -->

             <div class="box-body " >
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Name</th>
                       <th>Remove</th>
                       <th>update</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($authors->all() as $author)
                       <tr>
                         <td>{{$author->name}}</td>
                         <td><a href="{{url('techop/deleteauthor/'.$author->id)}}" class="btn btn-block btn-danger">remove</a></td>
                         <td><a href="{{url('techop/updateauthor/'.$author->id)}}" class="btn btn-block btn-warning">update</a><td>
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$authors->links()}}

          </div>
       </div>
       @endsection
