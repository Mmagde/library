
@extends('mviews.includes.main')
       @section('sidebar')
        @include('techop.sidebar')
       @endsection

@section('content')
<div class="content-wrapper">

  <div class="col-md-6" style="margin-top:20px;">

    @if (count($errors) > 0)
      <div class="alert alert-danger alert-dismissible" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
      </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <h4><i class="icon fa fa-check"></i> success</h4>
               {{ session('status') }}
        </div>
    @endif
    <form action="{{ url('/techop/addbook') }}" method="post" style="margin-top:20px;">
      {{csrf_field()}}
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Title" name="title">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Isbn" name="isbn">
      </div>
      <div class="form-group has-feedback">
       <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Authors <span class="caret"></span></button>
                <ul class="dropdown-menu scrollable-menu" role="menu" style=" height: auto;max-height: 200px;overflow-x: hidden;">
                  @foreach ($authors->all() as $author)
                  <li><input type="checkbox" value="{{$author->id}}" name="authors[]"><b>{{$author->name}} </b></li>
                  @endforeach
                </ul>
            </div>
      </div>
      <div class="form-group has-feedback">
        <textarea name="description" class="form-control" rows="8" cols="80">Describtion</textarea>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Edition" name="ed">
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Dewy decimal number" name="ddc">
      </div>
      <div class="form-group has-feedback">
        <input type="number" class="form-control" placeholder="Number of copies" name="numberofcopies">
      </div>

        <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register Book</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.form-box -->
  </div>

@endsection
