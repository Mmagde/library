
@extends('mviews.includes.main')
        @section('sidebar')
         @include('techop.sidebar')
        @endsection
       @section('content')

       <div class="content-wrapper">
         <div class="col-md-6" style="margin-top:20px;">
           @if (session('status'))
               <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i> success</h4>
                      {{ session('status') }}
               </div>
           @endif
               <h3 class="box-title">Books</h3>
             </div>

             <!-- /.box-header -->
             <style media="screen">
             .table {
               /*table-layout:fixed;*/
               table-layout: fixed;
               word-wrap: break-word;
              }

              .table td {
              /*white-space: nowrap;
              overflow-y: : scroll;
              text-overflow: ellipsis;
               word-wrap: break-word;*/

              }
             </style>
             <div class="box-body " >
               <table id="example2" class="table table-bordered table-hover">
                     <thead>
                     <tr>
                       <th>Title</th>
                       <th>Edition</th>
                       <th>ISBN</th>
                       <th>DDC</th>
                       <th>Description</th>
                       <th>Authors</th>
                       <th>Update</th>
                       <th>Delete</th>
                     </tr>
                     </thead>
                     <tbody>
                       @foreach ($books->all() as $book)
                       <tr>
                         <td>{{$book->title}}</td>
                         <td>{{$book->ed}}</td>
                         <td>{{$book->isbn}}</td>
                         <td>{{$book->ddc}}</td>
                         <td style="width:2000px;">
                           <!-- Button trigger modal -->
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$loop->index}}">
                             Description
                           </button>

                           <!-- Modal -->
                           <div class="modal fade" id="myModal{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                             <div class="modal-dialog" role="document">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel">Description</h4>
                                 </div>
                                 <div class="modal-body">
                                   {{$book->description}}
                                 </div>
                                 <div class="modal-footer">
                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </td>
                         <td>
                           @foreach ($book->book_author() as $book_author)
                               @if ($loop->last)
                                  {{$book_author->author()->name}}
                               @else
                                  {{$book_author->author()->name . ','}}
                               @endif
                           @endforeach
                         </td>
                         <td><a href="{{url('techop/deletebook/'.$book->id)}}" class="btn btn-block btn-danger">remove</a></td>
                         <td><a href="{{url('techop/updatebook/'.$book->id)}}" class="btn btn-block btn-warning">update</a><td>
                       </tr>
                       @endforeach

                     </tbody>

                   </table>
                   {{$books->links()}}

          </div>
       </div>

       @endsection
