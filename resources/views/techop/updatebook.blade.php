
@extends('mviews.includes.main')
       @section('sidebar')
        @include('techop.sidebar')
       @endsection

@section('content')
<div class="content-wrapper">

  <div class="col-md-6" style="margin-top:20px;">

    @if (count($errors) > 0)
      <div class="alert alert-danger alert-dismissible" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
      </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <h4><i class="icon fa fa-check"></i> success</h4>
               {{ session('status') }}
        </div>
    @endif
    <form action="{{ url('/techop/updatebook') }}" method="post" style="margin-top:20px;">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$book->id}}">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" value="{{$book->title}}" name="title">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" value="{{$book->isbn}}" name="isbn">
      </div>
      <div class="form-group has-feedback">
       <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Authors <span class="caret"></span></button>
                <ul class="dropdown-menu scrollable-menu" role="menu" style=" height: auto;max-height: 200px;overflow-x: hidden;">
                  @foreach ($authors as $author)
                    @if(in_array($author,$book_authors))
                       <li><input type="checkbox" value="{{$author->id}}" name="authors[]" checked><b>{{$author->name}}</b></li>
                    @else
                       <li><input type="checkbox" value="{{$author->id}}" name="authors[]" ><b>{{$author->name}}</b></li>
                    @endif
                  @endforeach
                </ul>
            </div>
      </div>
      <div class="form-group has-feedback">
        <textarea name="description" class="form-control" rows="8" cols="80">{{$book->description}}</textarea>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" value="{{$book->ed}}" name="ed">
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" value="{{$book->ddc}}" name="ddc">
      </div>
      <div class="form-group has-feedback">
        <input type="number" class="form-control" value="{{$book->numberofcopies}}" name="numberofcopies">
      </div>

        <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.form-box -->
  </div>

@endsection
