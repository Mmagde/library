<ul class="sidebar-menu">
  <li class="header">MAIN</li>
  <li class="treeview">
    <a href="#">
      <i class="fa "></i> <span>Books</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{url('/techop/addbook')}}"></i>Add new</a></li>
      <li><a href="{{url('/techop/books')}}"></i>Show</a></li>
   </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa "></i> <span>Authors</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{url('/techop/addauthor')}}"></i>Add new</a></li>
      <li><a href="{{url('/techop/authors')}}"></i>Show</a></li>
    </ul>
  </li>
</ul>
